from random import uniform, choice, randint


def right(x, y):
    return x + 1 if x < 2 else x, y


def left(x, y):
    return x - 1 if x > 0 else x, y


def up(x, y):
    return x, y - 1 if y > 0 else y


def down(x, y):
    return x, y + 1 if y < 2 else y


actions = [left, right, up, down]

illegal_actions = [(0, 0, left), (0, 0, up), (1, 0, up), (2, 0, up), (2, 0, right), (0, 1, left),
                   (0, 2, left), (0, 2, down), (1, 2, down), (2, 2, down), (2, 2, right), (2, 1, right)]

reward = {(0, 0, left): 100, (0, 0, right): 0, (0, 0, up): 100, (0, 0, down): 0,
          (0, 1, left): -1, (0, 1, right): 0, (0, 1, up): 100, (0, 1, down): 0,
          (0, 2, left): -1, (0, 2, right): 0, (0, 2, up): 0, (0, 2, down): -1,
          (1, 0, left): 100, (1, 0, right): 0, (1, 0, up): -1, (1, 0, down): 0,
          (1, 1, left): 0, (1, 1, right): 0, (1, 1, up): 0, (1, 1, down): 0,
          (1, 2, left): 0, (1, 2, right): 0, (1, 2, up): 0, (1, 2, down): -1,
          (2, 0, left): 0, (2, 0, right): -1, (2, 0, up): -1, (2, 0, down): 0,
          (2, 1, left): 0, (2, 1, right): -1, (2, 1, up): 0, (2, 1, down): 0,
          (2, 2, left): 0, (2, 2, right): -1, (2, 2, up): 0, (2, 2, down): -1, }


def print_q(q):
    visualization = '-' * 37 + '\n'
    for y in range(3):
        for arrow, action in [('←', left), ('→', right), ('↑', up), ('↓', down)]:
            for x in range(3):
                visualization += '| {}: {:6.1f} '.format(arrow, q[(x, y, action)])
            visualization += '|\n'
        visualization += '-' * 37 + '\n'
    print(visualization)


class Agent:
    def __init__(self, gamma=0.95, exploration_rate=0.9):
        self.gamma = gamma
        self.exploration_rate = exploration_rate
        self.Q = {(0, 0, left): 0, (0, 0, right): 0, (0, 0, up): 0, (0, 0, down): 0,
                  (0, 1, left): 0, (0, 1, right): 0, (0, 1, up): 0, (0, 1, down): 0,
                  (0, 2, left): 0, (0, 2, right): 0, (0, 2, up): 0, (0, 2, down): 0,
                  (1, 0, left): 0, (1, 0, right): 0, (1, 0, up): 0, (1, 0, down): 0,
                  (1, 1, left): 0, (1, 1, right): 0, (1, 1, up): 0, (1, 1, down): 0,
                  (1, 2, left): 0, (1, 2, right): 0, (1, 2, up): 0, (1, 2, down): 0,
                  (2, 0, left): 0, (2, 0, right): 0, (2, 0, up): 0, (2, 0, down): 0,
                  (2, 1, left): 0, (2, 1, right): 0, (2, 1, up): 0, (2, 1, down): 0,
                  (2, 2, left): 0, (2, 2, right): 0, (2, 2, up): 0, (2, 2, down): 0, }

    def max_next_q_value(self, x, y):
        next_states = [action(x, y) for action in actions]
        all_qs = []
        for action in actions:
            all_qs += [self.Q[(state[0], state[1], action)] for state in next_states]
        return max(all_qs)

    def choose_greedy_action(self, x, y):
        q_values_actions = [(self.Q[x, y, action], action) for action in actions
                            if (x, y, action) not in illegal_actions]
        return choice([action for q_value, action in q_values_actions
                       if max(q_values_actions, key=lambda z: z[0])[0] == q_value])

    def train(self, episodes=1):
        for _ in range(episodes):
            x, y = randint(0, 2), randint(0, 2)
            while not (x == 0 and y == 0):
                action = choice(actions) if uniform(0, 1) >= self.exploration_rate \
                    else self.choose_greedy_action(x, y)
                next_x, next_y = action(x, y)
                self.Q[(x, y, action)] = reward[x, y, action] + \
                                         self.gamma * self.max_next_q_value(next_x, next_y)
                x, y = next_x, next_y

    def run(self, x, y):
        action_counter = 0
        while not (x == 0 and y == 0):
            action = self.choose_greedy_action(x, y)
            x, y = action(x, y)
            action_counter += 1
        return action_counter


if __name__ == '__main__':
    agent = Agent()
    agent.train(episodes=2000)
    print_q(agent.Q)
    for start_x, start_y in [(2, 2), (2, 1), (1, 1)]:
        print('Start:({},{}) It took {} actions to the goal.'
              .format(start_x, start_y, agent.run(start_x, start_y)))
