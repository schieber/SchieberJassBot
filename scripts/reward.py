MAX_POINTS = 55
MIN_POINTS = -55


def calculate_reward(points_team1, points_team2, done):
    if done:
        if points_team1 > points_team2:
            return 2.5
        elif points_team1 == points_team2:
            return 0
        else:
            return -2.5
    if points_team1 > points_team2:
        return normalize_reward(points_team2)
    if points_team1 == points_team2:
        return 0
    return -normalize_reward(points_team2)


def normalize_reward(points):
    return (points - MIN_POINTS) / (MAX_POINTS - MIN_POINTS)
