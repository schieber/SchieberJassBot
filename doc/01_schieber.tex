\chapter{Schieber}
Schieber is the name of a famous Swiss card game. It is the most common variant of \gls{Jass} in Switzerland.
Schieber Jass is played with four people.
They are split into two teams of two people each and compete against each other.
It is played with a deck of 36 cards divided into four different suits of 9 cards each.
One suit includes the cards 6, 7, 8, 9 (\gls{Nell}), 10 (\gls{Banner}), Unter, Ober, König and As.
The suits of the Swiss-German playing cards are Schellen, Rosen, Schilten and Eichel (Other decks like the French or the Austrian are also suitable).
The goal of the game is to reach a certain amount of points before your opponents. Often the point limit is set to 1000 or 2500.

\section{Game description}
First, the four player have to build the two teams.
Then they  sit around a table in a defined order.
Your teammate does sit across from you and on your right and left there are your opponents.

At the beginning of a round one player shuffles the deck and deals all the cards. This leads to 9 cards for every player.
Hence, every round consists of 9 Stiche.
In a \gls{Stich} every player has to lay down one card and the team with the strongest card, related to the rules of the game, gets all the points of the cards on the table.
That results in a total of 157 points per round.

Next, the player on the right hand side of the dealer, also called \gls{Vorhand}, has to choose the \gls{Trumpf}.
Possible Trumpfs are the four different suits, \gls{Obenabe} or \gls{Undenufe}.
If the \gls{Vorhand} player can't decide on a Trumpf, he has the ability to schieben \footnote{The name of the game originated from the verb schieben}, which means that his teammate has to choose a Trumpf.

After the Trumpf has been chosen, the \gls{Vorhand} player has to choose a card and open the first \gls{Stich}.
Right before every player has laid his first card on the table they can \gls{Wies}, which describes the announcement of a sequence of cards to get additional points.

Now, each player in turn can choose the allowed cards depending on the rules of the game.
The card choosing order is in reverse clockwise direction.
When everyone has played a card, the highest card wins and the team gets all the points of the \gls{Stich}.
The player who won the \gls{Stich} is now in charge of playing the first card of the new \gls{Stich}.
When all cards are played, a round is over and the \gls{Vorhand} player becomes the dealer.
The new \gls{Vorhand} player is the one on the right hand side of the dealer as known.

The whole process repeats until one of the teams reach the point limit.

For a more detailed description of the game composition and the rules I recommend having a look at detail description in PAGAT \cite{schieberJassRules} or the basics listed on the Swisslos homepage \cite{jassBasics}.


\newpage

\section{Schieber Jass combinatorics}
To get an impression of the state space of the Schieber Jass game, let's consider the combinatorics.

\subsection{Card distribution}
If we think about card distribution in Schieber Jass, we are talking about a combination \textbf{without repetition} and \textbf{no interest in the order of the elements}.
This is known as the binomial coefficient. The equation is listed in \eqref{eq:binomial}.

\begin{equation}
  \begin{gathered}
  \left(
    \begin{array}{c}
      n \\
      k
    \end{array}
  \right) = \frac{n!}{k!(n-k)!} \\
  \end{gathered}\label{eq:binomial}
\end{equation}
where:
\begin{itemize}[noitemsep, topsep=0pt]
\itemsep-0.5em
 \item[n:]  number of all elements in the set
 \item[k:]  number of subset elements
\end{itemize}

\subsubsection{Deal cards}
Remember, we are faced with the following circumstances:

\begin{itemize}[noitemsep, topsep=0pt]
\itemsep-0.5em
 \item[] n = 36 (Number of cards)
 \item[] k = 9  (Number of cards per player)
\end{itemize}


The calculation of all combinations with which all the cards can be dealt at the beginning of the game is listed in \eqref{eq:allcards}.

\begin{equation}
  \begin{gathered}
  \left( \begin{array}{c} 36 \\ 9 \end{array} \right)
  \left( \begin{array}{c} 27 \\ 9 \end{array} \right)
  \left( \begin{array}{c} 18 \\ 9 \end{array} \right)
  \left( \begin{array}{c} 9 \\ 9 \end{array} \right)
  = 2.145275226626532 \times 10^{19}\\
  \end{gathered}\label{eq:allcards}
\end{equation}

As you can see, the possible combinations are pretty numerous.
However, if we focus on the strategy of one particular player, we know our own hand cards and the combinations reduce.
There are 27 remaining unknown cards. The resulting combinations are illustrated in equation \eqref{eq:unknown}.
This represents the situation at the beginning of the first \gls{Stich} of a round.

\begin{equation}
  \begin{gathered}
  \left( \begin{array}{c} 27 \\ 9 \end{array} \right)
  \left( \begin{array}{c} 18 \\ 9 \end{array} \right)
  \left( \begin{array}{c} 9 \\ 9 \end{array} \right)
  = 2.278734315 \times 10^{11}\\
  \end{gathered}\label{eq:unknown}
\end{equation}

If we go ahead in the game, we are able to calculate the possible card distribution at the beginning of every \gls{Stich}, you can see this in the equation listed in \eqref{eq:stich}.
What is even more interesting is calculating the possible card distribution at the moment when you have to choose a card.
The computation for the combinations at the beginning of the \gls{Stich} is listed in table \ref{tab:stichcombinations}

\begin{equation}
  \begin{gathered}
  \prod\limits_{i=1}^3 \left( \begin{array}{c} k * i \\ k \end{array} \right)\\
  \end{gathered}\label{eq:stich}
\end{equation}
where:
\begin{itemize}[noitemsep, topsep=0pt]
\itemsep-0.5em
 \item[k:]  number of hand cards
 \item[i:]  counter, representing the opponents and the teammate
\end{itemize}


\begin{table}[H]
\centering
\begin{tabular}{ | m{1cm} | m{2.5cm} | m{2.5cm} | m{4.5cm} | m{2.5cm}  m{0cm} |}
\hline
\rowcolor{lightblue}
Stich & hand cards & unknown cards & calculation & combinations & \\
\hline
1 & 9 & 27 &  $\left( \begin{array}{c} 27 \\ 9 \end{array} \right) \left( \begin{array}{c} 18 \\ 9 \end{array} \right) \left( \begin{array}{c} 9 \\ 9 \end{array} \right)$ & 227873431500 & \\ [22pt] \hline
2 & 8 & 24 & $\left( \begin{array}{c} 24 \\ 8 \end{array} \right) \left( \begin{array}{c} 16 \\ 8 \end{array} \right) \left( \begin{array}{c} 8 \\ 8 \end{array} \right)$ & 9465511770 & \\ [22pt] \hline
3 & 7 & 21 & $\left( \begin{array}{c} 21 \\ 7 \end{array} \right) \left( \begin{array}{c} 14 \\ 7 \end{array} \right) \left( \begin{array}{c} 7 \\ 7 \end{array} \right)$ & 399072960 & \\ [22pt] \hline
4 & 6 & 18 & $\left( \begin{array}{c} 18 \\ 6 \end{array} \right) \left( \begin{array}{c} 12 \\ 6 \end{array} \right) \left( \begin{array}{c} 6 \\ 6 \end{array} \right)$ & 17153136 & \\ [22pt] \hline
5 & 5 & 15 & $\left( \begin{array}{c} 15 \\ 5 \end{array} \right) \left( \begin{array}{c} 10 \\ 5 \end{array} \right) \left( \begin{array}{c} 5 \\ 5 \end{array} \right)$ & 756756 & \\ [22pt] \hline
6 & 4 & 12 & $\left( \begin{array}{c} 12 \\ 4 \end{array} \right) \left( \begin{array}{c} 8 \\ 4 \end{array} \right) \left( \begin{array}{c} 4 \\ 4 \end{array} \right)$ & 34650 & \\ [22pt] \hline
7 & 3 & 9 &  $\left( \begin{array}{c} 9 \\ 3 \end{array} \right) \left( \begin{array}{c} 6 \\ 3 \end{array} \right) \left( \begin{array}{c} 3 \\ 3 \end{array} \right)$ & 1680 & \\ [22pt] \hline
8 & 2 & 6 &  $\left( \begin{array}{c} 6 \\ 2 \end{array} \right) \left( \begin{array}{c} 4 \\ 2 \end{array} \right) \left( \begin{array}{c} 2 \\ 2 \end{array} \right)$ & 90 & \\ [22pt] \hline
9 & 1 & 3 &  $\left( \begin{array}{c} 3 \\ 1 \end{array} \right) \left( \begin{array}{c} 2 \\ 1 \end{array} \right) \left( \begin{array}{c} 1 \\ 1 \end{array} \right)$ & 6 & \\ [22pt]
\hline
\end{tabular}
\caption{combinations during the individual Stiche}
\label{tab:stichcombinations}
\end{table}

Since the amount of combinations are still huge at the beginning of the game, we should consider possibilities to reduce it.
An obvious approach is to consider the rules of the game and use the fact that not every card is allowed at every point of the game.
Thus, we can calculate the allowed cards of all the yet unknown cards during the gameplay.
The math representing that process is out of the scope of this project.
But to get an assumption of how much the combinations are reduced, I used the Monte Carlo method and simulated 100'000 Schieber Jass rounds to count the amount of allowed cards during the individual Stiche.
The result of this process is listed in table \ref{tab:montecarlo}.

\begin{table}[H]
\centering
\begin{tabular}{  |l|l|l|l|l|}
\hline
\rowcolor{lightblue}
Stich & max & min & unknown & average \\
\hline
1 & 27 & 1 & 27 & 12.5 \\ \hline
2 & 24 & 1 & 24 & 11.1 \\ \hline
3 & 21 & 1 & 21 & 9.7 \\ \hline
4 & 18 & 1 & 18 & 8.5 \\ \hline
5 & 15 & 1 & 15 & 7.2 \\ \hline
6 & 12 & 1 & 12 & 6.0 \\ \hline
7 & 9 & 1 & 9 & 4.8 \\ \hline
8 & 6 & 1 & 6 & 3.4 \\ \hline
9 & 3 & 0 & 3 & 1.4 \\ \hline
\end{tabular}
\caption{100'000 simulated Schieber Jass rounds}
\label{tab:montecarlo}
\end{table}

As you can see from table \ref{tab:montecarlo}, the average number of possible cards is about half the amount of all the unknown cards.
If we take the first \gls{Stich} and round the 12.5 up to 13, we only receive 715 possible combinations, as illustrated in \eqref{eq:reduction}.

\begin{equation}
  \begin{gathered}
  \left( \begin{array}{c} 13 \\ 9 \end{array} \right)\left( \begin{array}{c} 9 \\ 9 \end{array} \right) \left( \begin{array}{c} 9 \\ 9 \end{array} \right) = 715 \\
  \end{gathered}\label{eq:reduction}
\end{equation}


\subsection{Game play}
After the card has been dealt the game can start. Hence, the next question is, "How many possible sequences are there to play the cards?"
For this kind of arrangement problem, \textbf{the order of the card is relevant and there are no repetitions}.
Thus, we could apply the factorial illustrated in \eqref{eq:arrange}

\begin{equation}
  \begin{gathered}
  n! \\
  \end{gathered}\label{eq:arrange}
\end{equation}
where:
\begin{itemize}[noitemsep, topsep=0pt]
\itemsep-0.5em
 \item[n:]  number of all elements in the set
\end{itemize}

The game starts with 36 cards and they can be arrange 36! different ways. That is $3.719933267899012174679994481508352 \times 10^{41}$.
If we apply the factorial of the number of cards at the beginning of every \gls{Stich}, we get the permutations listed in table \ref{tab:permutations}.

\begin{table}[H]
\centering
\begin{tabular}{ |l|l|l|l|l|}
\hline
\rowcolor{lightblue}
Stich & cards & permutations \\
\hline
1 & 36 & 371993326789901000000000000000000000000000 \\ \hline
2 & 32 & 263130836933694000000000000000000000 \\ \hline
3 & 28 & 304888344611714000000000000000 \\ \hline
4 & 24 & 620448401733239000000000 \\ \hline
5 & 20 & 2432902008176640000 \\ \hline
6 & 16 & 20922789888000 \\ \hline
7 & 12 & 479001600 \\ \hline
8 & 8 & 40320 \\ \hline
9 & 4 & 24 \\ \hline
\end{tabular}
\caption{number of permutations at the beginning of every Stich}
\label{tab:permutations}
\end{table}

In contrast to the card distribution, we can't subtract our own hand cards.
But the rules of the game still limit the number of allowed playable cards.
Hence, we reapply the Monte Carlo method to get an impression how many cards are allowed at the moment when you have to choose a card.
I simulated 100'000 Schieber Jass rounds again and counted all the currently allowed unplayed cards. The result is listed in table \ref{tab:allowedcardsgameplay}.

\begin{table}[H]
\centering
\begin{tabular}{ |l|l|l|l|}
\hline
\rowcolor{lightblue}
Stich & max & min & average \\
\hline
1 & 36 & 6 & 17.0 \\ \hline
2 & 32 & 2 & 15.5 \\ \hline
3 & 28 & 1 & 12.8 \\ \hline
4 & 24 & 1 & 11.2 \\ \hline
5 & 20 & 1 & 9.8 \\ \hline
6 & 16 & 1 & 7.8 \\ \hline
7 & 12 & 1 & 6.2 \\ \hline
8 & 8 & 1 & 4.3 \\ \hline
9 & 4 & 1 & 2.2 \\ \hline
\end{tabular}
\caption{number allowed cards during the gameplay}
\label{tab:allowedcardsgameplay}
\end{table}

Even if we only take the average of the allowed cards, we end up wit 17! at the beginning of the game. At \gls{Stich} 5 we are still at about 10! (3'628'800) permutations. \\
This leads to the assumption that in a lot of the cases it is also a hard task for a computer to calculate all the possible runs of the play in a reasonable amount of time.