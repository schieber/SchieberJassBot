\chapter{Approach}
The following sections give an overview of the proceedings of the term project, list problems occurred and presents approaches that helped to solve them.

\section{Summary}
At the beginning of the project, I started by building a Schieber Jass environment to play and train a reinforcement learning \gls{bot}.
This resulted in the pyschieber mentioned in section \ref{pyschieber}.
pyschieber enabled the implementation of a deep Q-learning \gls{bot} to compete against a random, a greedy and a challenge winning player.
The first attempts to learn and win the Schieber Jass game weren't very successful and two deep Q-learning bots together weren't able to win against the two opponents with a random playing strategy.
Since I have never used reinforcement learning techniques and I didn't know if the Schieber Jass is too complex or if I would have uncertainties in the implementation of the deep Q-learning \gls{bot}.
I decided to apply the same approach to Tic-Tac-Toe a less sophisticated game, as a proof of concept.
For that purpose, I developed pytictactoe as you can see in section \ref{pytictactoe} and had the first successes winning against a random \gls{bot}.
Further, I was able to win against a \gls{bot} with a defensive strategy.
After the successes with Tic-Tac-Toe, I tried again to train a neural network that is able to play Schieber Jass.
Unfortunately, even with the testing of different information about the state, several model configurations as well as different hyperparameters for the neural network, there was no success at all.
This resulted in the idea of minifying the Schieber Jass game without changing too much of the gameplay.
Hence, there still has to be multiple opponents, a teammate and choices for illegal actions to take.
This resulted in the pyMiniJass game, listed in \ref{pyMiniJass}.
The pyMiniJass game seemed to be easy enough to find a simple strategy to win against a random \gls{bot}.
Several tries didn't led to a competitive capable reinforcement learning \gls{bot} again.
Only when I changed the opponents and the teammate to greedy players, I did achieve the first positive results.
In relation to these circumstances it sounds logical that learning from random behavior was not be purposeful.
The knowledge gained from the pyMiniJass were applied at the Schieber Jass and quickly made first successes.
The random and the greedy bots could been beaten.

\newpage
\section{Problems}
The game Schieber Jass has got several hurdles compared to other deep Q-learning success stories.
Other attempts, like the one of Volodymyr Mnih et al. \cite{mnih2013playing} are playing Atari games.
In comparison, Schieber Jass is an imperfect but complete information game.
It has got multiple opponents and you have a teammate.

\subsection{Reward}
\paragraph{Description}
In Schieber Jass, it is a hard task to define an adequate reward function,
because you are not fully in charge of the outcome of the game.
For instance, if you have only bad cards on your hand but your teammate is able to win all Stiche, you still win the game.
You can help to influence the game if you play with cards of high value at the right moment. This process is called \gls{schmieren}, but that is complicated to rate.

\paragraph{Approach}
In reinforcement learning, you often read about getting a positive reward if you win the game or a negative reward if you lose.
The actions during the game are rated with zero reward.
Unfortunately, this approach wasn't sufficient for the Schieber Jass game.
For instance you could play very well but lose the game because of to the bad cards you got.
Hence, the next idea I tried was to use the gained or lost points of every Stich as the reward.
This attempt led to better results.
To upgrade this approach, I normalized the gained points and added special reward if you win or lose the whole round.
The resulting function is listed in \ref{lst:rewardfunction}.
You get a positive reward whenever your team wins and not only if you play the highest card.
Furthermore, if your teammate does the \gls{Stich} and you support it with a high value card, you get a larger reward.

\begin{lstlisting}[label=lst:rewardfunction, language=iPython, caption={reward function},captionpos=b]
MAX_POINTS = 55
MIN_POINTS = -55


def calculate_reward(points_team1, points_team2, done):
    if done:
        if points_team1 > points_team2:
            return 2.5
        elif points_team1 == points_team2:
            return 0
        else:
            return -2.5
    if points_team1 > points_team2:
        return normalize_reward(points_team2)
    if points_team1 == points_team2:
        return 0
    return -normalize_reward(points_team2)


def normalize_reward(points):
    return (points - MIN_POINTS) / (MAX_POINTS - MIN_POINTS)
\end{lstlisting}



\subsection{Game state}
\paragraph{Description}
A complicated decision is to provide the game state for the neural network.
First, it is common to use \gls{one-hot encoding} to represent the input for the model.
This leads to 36 values for the deck and avoids giving preference to certain cards or Trumpfs.
For example, you could decode the 4 players, the 6 \gls{Trumpf} decisions and all the 9 rounds to end up with $4 \cdot 36 \cdot 9 + 6 = 1302$ input values.
This is illustrated in figure \ref{fig:input}.


\begin{figure}[H]
    \centering
    \includegraphics[width=1.0\textwidth,keepaspectratio]{images/input.png}
        \caption{illustration of the input for the neural network, a black rectangle represents a one and a white one a zero}
        \label{fig:input}
\end{figure}

There are a lot of possible combinations greatly varying in numbers of values to represent the game state.
The problem was to find an accurate representation for the input.
If the number of input values is too high, it is may be too difficult to train the \gls{bot} and if you provide too sparse information, the \gls{bot} won't learn anything either.
Furthermore, it could also be helpful to use redundant information in the input as an indication of the importance of some values.


\paragraph{Approach}
Figuring out a suitable representation of the game state for the input of the neural network was a difficult task.
The problem was to find a good balance between not to much information but still enough to represent the problem.
After several attempts, I achieved the best results using the hand cards, the table, cards for all the players and the \gls{Trumpf} decision.
The total number of input values is $6 \cdot 36 \cdot + 6 = 222$.
A visualization is shown in figure \ref{fig:inputresult}.

\begin{figure}[H]
    \centering
    \includegraphics[width=1.0\textwidth,keepaspectratio]{images/inputresult.png}
        \caption{illustration of the resulting input for the neural network}
        \label{fig:inputresult}
\end{figure}

\subsection{Handle invalid actions}
\paragraph{Description}
The input layer of the neural network gets the game state and depending on it the output layer proposes the card to play.
The output layer consists of 36 values representing the 36 possible cards of the game.
It uses a linear activation function and the highest proposed value is used as the chosen card.
Unfortunately, the neural network is able to rate a unallowed card as the best fit.
Thus, the question of how to handle such situations arises.
Possible approaches are:
\begin{itemize}
    \item Ignore the action and do nothing. For Schieber Jass this isn't an option since you always have to choose a card, but it is a common practice in reinforcement learning.
    \item Rate the action negative, retrain and let the network predict again until an allowed card is proposed.
    \item Choose the highest rated allowed card of the prediction.
\end{itemize}


\paragraph{Approach}
During the term project, I tested the variants of choosing the highest rated allowed card of the prediction and of rating the action negative.
Both methods seemed to work, but there where significant differences in performance.
Retraining the neural network was a time consuming task, which led to the method of choosing the highest rated allowed card.


For further information, a discussion about the problem can be found on StackExchange:
\begin{itemize}
    \item How to use DQN to handle an imperfect but complete information game? \cite{handledqn}
\end{itemize}


\subsection{Player constellation}
\paragraph{Description} Getting the first learning Schieber Jass deep Q-learning \gls{bot} was a complicated process.
Initially, I used two learning deep Q-learning bots as teammates against two random players.
Unfortunately there was no success at all for a long time.
Then somewhen the question of whether it is a reasonable strategy to learn from random opponents arose.
Thus, other player constellations were tested.

\paragraph{Approach}
The first improvements were achieved when the teammate and the opponents player type were changed.
Table \ref{tab:train} illustrates the teams to train the reinforcement learning \gls{bot}
where Player 1 and Player 3 team up and Player 2 and Player 4 build the opponent team.


\begin{table}[H]
\centering
\begin{tabular}{ | p{2.5cm} | p{2.5cm} |p{2.5cm} |p{2.5cm} |p{3cm} |}
\hline
\rowcolor{lightblue}
Player 1 & Player 2 & Player 3 & Player 4 & Conclusion  \\
\hline
Learning bot & Random bot & Learning bot & Random bot & No learning \\ \hline
Learning bot & Greedy bot & Greedy bot & Greedy bot & Starts to learn \\
\hline
\end{tabular}
\caption{teams to train}
\label{tab:train}
\end{table}

Theoretically, the reinforcement learning \gls{bot} should be able to learn how to win Schieber Jass against random playing opponents and teammates.
But maybe it needs too much time and too many simulated games.
Nevertheless, a deep Q-learning \gls{bot} was able to learn from players with a stable playing strategy in a reasonable amount of time.

\subsection{Model configuration}
\paragraph{Description}
One of the most time consuming tasks of a machine learning engineer is the configuration of your model and tuning of your hyperparameters.
Often you start with a simple model and observe the behavior.
If the neural network starts to recognize the problem and begins to learn you are on the right track. From then on, you can try different configurations and tune all the parameters.
Known techniques are:

\begin{itemize}
    \item Adding regularization to avoid overfitting (dropout, L1, L2)
    \item Change the number of layers
    \item Try different hyperparameters (units per layer, learning rate, optimizer, activation function)
    \item Use a tool like Hyperas \cite{hyperas} to automate the hyperparameter optimization
\end{itemize}

\paragraph{Approach}
To figure out if a neural network is going to learn and is able to beat an opponent, I simulated about 200'000 Schieber Jass rounds
and tracked how often the \gls{bot} won during the last 1000 games.
This was done to observe trends during the learning phase.
If the \gls{bot} didn't get better after a certain period of time, I stopped the process and applied changes on the model and the hyperparameters.

The best performing model against the greedy \gls{bot} is illustrated in table \ref{tab:bestmodelgreedy}.
If we sum up all the parameters of the model, we end up with a total of 273'451 trainable parameters.
It uses a Nesterov Adam optimizer, rectifier activation functions and a mean squared error \gls{loss function}.
An interesting fact is the usage of 1D convolutional layers. They help to group together multiple input values.

My assumption is that the grouping together of information, like the 36 values for the cards on the table, improves the performance and led to a simple strategy that is able to beat the greedy opponent.  \\

\begin{table}[H]
\centering
\begin{tabular}{| p{4.5cm} | p{4.5cm} | p{4.5cm} |}
\hline
\rowcolor{lightblue}
Layer (type)  & Output Shape & Number of parameters  \\
\hline
dense\_1 (Dense)      &        (None, 444)   &            99012    \\ \hline
reshape\_1 (Reshape)    &      (None, 444, 1)        &    0     \\ \hline
conv1d\_1 (Conv1D)       &     (None, 50, 50)        &    500      \\ \hline
conv1d\_2 (Conv1D)        &    (None, 6, 50)          &   45050     \\ \hline
conv1d\_3 (Conv1D)         &   (None, 1, 50)     &        90050    \\ \hline
conv1d\_4 (Conv1D)     &       (None, 1, 25)    &         11275       \\ \hline
flatten\_1 (Flatten)     &     (None, 25)       &         0        \\ \hline
dense\_2 (Dense)        &      (None, 444)       &        11544          \\ \hline
dense\_3 (Dense)         &     (None, 36)         &       16020         \\ \hline
\end{tabular}
\caption{best model against greedy opponents}
\label{tab:bestmodelgreedy}
\end{table}

For the challenge winning \gls{bot} as opponent another model configuration led to the best results.
It uses fully connected layers, a Nesterov Adam optimizer, rectifier activation functions and a mean squared error \gls{loss function}.
Resulting in 16'396'068 trainable parameters.
The layering is represented in table \ref{tab:bestmodelchallenge}.
The high number of parameters slows down the training phase, but enables to store more information and helped to achieve the best results against the challenge winning opponents.

\begin{table}[H]
\centering
\begin{tabular}{| p{4.5cm} | p{4.5cm} | p{4.5cm} |}
\hline
\rowcolor{lightblue}
Layer (type)  & Output Shape & Number of parameters  \\
\hline
dense\_1 (Dense)      &        (None, 1110)    &            247530    \\ \hline
dense\_2 (Dense)     &      (None, 1554)        &    1726494     \\ \hline
dense\_3 (Dense)        &     (None, 1998)        &    3106890      \\ \hline
dense\_4 (Dense)        &     (None, 2886)        &    5769114      \\ \hline
dense\_5 (Dense)        &     (None, 1554)        &    4486398      \\ \hline
dense\_6 (Dense)        &     (None, 666)        &    1035630      \\ \hline
dense\_7 (Dense)        &     (None, 36)        &    24012      \\ \hline
\end{tabular}
\caption{best model against challenge winning opponents}
\label{tab:bestmodelchallenge}
\end{table}

\subsection{Trumpf selection}
\paragraph{Description}
In addition to the card choices during the gameplay sometimes you have to select a \gls{Trumpf} or you can schieben.
The \gls{Trumpf} selection is very relevant for the outcome of the game but is comparatively difficult to rate.
For example, you can choose a good \gls{Trumpf} in relation to the cards of your hand but if you aren't able to play with an appropriate strategy you will lose the game anyway.
And the other way around is as important as well.
If you chose a bad \gls{Trumpf} even the best player isn't able to win the round.

\paragraph{Approach}
This led me to the decision of focusing on the gameplay with the reinforcement learning \gls{bot} and taking a fix strategy to choose the \gls{Trumpf}.
The chosen strategy is based on the following blog entry:
\begin{itemize}
    \item Jass-Tipp: Richtig Trumpf ansagen beim Schieber \cite{trumpfwahl}
\end{itemize}