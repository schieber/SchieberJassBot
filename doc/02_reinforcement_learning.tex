\chapter{Reinforcement Learning}
\label{ReinforcementLearning}

Reinforcement learning is a computational approach to learn how to act under certain circumstances.
The goal is to optimize the behavior of an \textbf{agent} by getting only the state and a reward from the \textbf{environment}.
Hence, the main components of reinforcement learning are the environment and the agent.
The environment represents the state, the possible transitions and should provide some kind of a reward.
The agent is the learning part of the system and decides which action to take in relation to the state of the environment.
In response to the action the agent gets a reward from the environment. This process is illustrated in figure \ref{fig:rl}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.4\textwidth,keepaspectratio]{images/rl.png}
        \caption{Reinforcement Learning}
        \label{fig:rl}
\end{figure}


Furthermore, reinforcement learning contains a policy, a reward, a value function and optionally a model of the environment.

\textit{The policy, $\pi$,} defines the behavior of the agent at a certain moment depending on the current state and all the possible actions.
It could be seen as a mapping from a state to an action.

\textit{The reward} describes the goal of the problem. It is a numeric value often provided by the environment and it is given immediately after every action.
Based on the reward, we update and improve the policy.

\textit{The value function, $\upsilon$,} determines how good it is to be in a particular state.
In contrast to the reward, the value function doesn't only depend on the current action.
It does a rating based on experience and tries to include the estimated following reward.

\textit{The model} is not an absolute necessary part of a reinforcement learning system
but often it is very essential to taking good decisions.
The aim of a model is to simulate the behavior of the environment.
This simulation supports the planning mechanism by getting information about possible future situations that have not yet happened.


\section{Terminology}
The current section gives an overview of common concepts of reinforcement learning.
For more information I recommend Richard Sutton's and Andrew Barto's excellent book \cite{sutton1998reinforcement} or the reinforcement learning course by David Silver \cite{rlcourse}.

\subsection{Markov Decision Process}
A markov decision process is a method to formally describe  an environment for reinforcement learning.
It is a stochastic process that specifies transition probabilities from state to state.
The objective of a markov decision process is to find a strategy to maximize the sum of future rewards.
Further, the environment of a markov decision process is fully observable and it fulfills the markov property.
The following list formalizes a markov decision process:

\begin{itemize}
    \item $S$ a finite set of all states
    \item $A$ a finite set of all actions, the transitions between the states
    \item $R$ reward associated with each transition
    \item $\gamma \in [0,1]$ discount factor, quantifies the difference in importance between immediate rewards and future rewards
\end{itemize}

\paragraph{Markov Property}
The markov property means that the underlying process is memoryless \cite{doob1953stochastic}.
This implies that the current state contains all the information about the history.
Or in other words, all the future states and actions are independent on the past.

\subsubsection{Example}
Markov decision processes can be illustrated similarly to markov chains, also known as markov processes.
But markov decision processes extend the markov chain with actions and rewards. An example is illustrated in figure \ref{fig:mdpDownhill}.
It shows a possible drive along a downhill track with different actions to take.
Depending on those actions there are different probabilities to reach the goal or unfortunately go to the hospital.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth,keepaspectratio]{images/mdpDownhill.png}
        \caption{a markov decision process at the example of a downhill ride}
        \label{fig:mdpDownhill}
\end{figure}

In figure \ref{fig:mdpDownhill} the actions are illustrated as the blue circles, the states are represented by the yellow circles, the probabilities are written in black and the rewards are written in red.

\subsection{Episode}
If we have a look at an arbitrary problem and formalize the process as a markov decision process we get a repeated sequence of states, actions and rewards until a terminal state is reached,  this is called an episode.
An example is listed in \eqref{eq:episode}.

\begin{equation}
  \begin{gathered}
   S_{0},A_{0},R_{0},S_{1},A_{1},R_{1},S_{2},A_{2},R_{2}, ... ,S_{n}
  \end{gathered}\label{eq:episode}
\end{equation}
where:
\begin{itemize}[noitemsep, topsep=0pt]
\itemsep-0.5em
 \item[n:]  terminal state
\end{itemize}

\paragraph{Example} In terms of our downhill example a possible episode is listed in \eqref{eq:episodeexample}.

\begin{equation}
  \begin{gathered}
   S_{Start},A_{drive\_risky},R_{1},S_{Hospital},A_{have\_surgery},R_{4},S_{Goal}
  \end{gathered}\label{eq:episodeexample}
\end{equation}


\subsection{Reward}
As already mentioned, the goal of an agent is to maximize the cumulative received reward during an episode.
This is based on the idea of the reward hypothesis \cite{rewardhypothesis}:
  \begin{quote}
   That all of what we mean by goals and purposes can be well thought of as maximization of the expected value of the cumulative sum of a received scalar signal (reward).
  \end{quote}

The total reward of an episode can be obtained by applying the equation listed in \eqref{eq:totalreward}.

\begin{equation}
  \begin{gathered}
   R_{total} = \sum_{t=0}^{n} R_{t}
  \end{gathered}\label{eq:totalreward}
\end{equation}

\paragraph{Example} If we take the episode from our example in \eqref{eq:episodeexample}, we get get the result that you see in \eqref{eq:totalrewardexample}.

\begin{equation}
  \begin{gathered}
   R_{total} = R_{1} + R_{4} = -3 + 1 = -2
  \end{gathered}\label{eq:totalrewardexample}
\end{equation}

\subsubsection{Discounted reward}
In reinforcement learning there often is a discount factor $\gamma$ used to estimate the future reward.
Reasons to use a discount factor are uncertainties about the future, avoiding infinity cycles in a markov process and there is a psychological effect as well. Humans tend to favor immediate reward over future one.
The calculation of the discounted reward of an episode is illustrated in \eqref{eq:yreward}.
\begin{equation}
  \begin{gathered}
   R_{discounted} = \sum_{t=0}^{n} \gamma^{t} R_{t}
  \end{gathered}\label{eq:yreward}
\end{equation}

\paragraph{Example} Now imagine you are at the beginning of the downhill track and you think that your ride will end like the episode of example \eqref{eq:episodeexample}.
Since you are not absolutely sure, you consider a discount factor $\gamma$ in your calculation and set it to 0.9, resulting in \eqref{eq:yrewardexample}.
\begin{equation}
  \begin{gathered}
   R_{discounted} = \gamma^{0} R_{1} + \gamma^{1} R_{4} = 0.9^{0} \cdot (-3) + 0.9^{1} \cdot 1 = -2.1
  \end{gathered}\label{eq:yrewardexample}
\end{equation}


\subsection{Value Functions}
Reinforcement learning algorithms involve estimating value functions.
Value functions are always dependent on a certain policy, $\pi$. There are two different functions we have to look at:
\begin{itemize}
    \item state-value-function
    \item action-value-function
\end{itemize}

\subsubsection{State-Value-Function}
The state-value-function defines the expected value of the state $S$ by following the policy $\pi$ and estimates how good it is to be in a certain state.
\begin{equation}
  \begin{gathered}
   V^{\pi}(s)= E_{\pi} \bigg[ \sum_{k=0}^{\infty} {\gamma}^{k} R_{t+k+1} \mid S_{t} = s\bigg]
  \end{gathered}\label{eq:afunction}
\end{equation}
where:
\begin{itemize}[noitemsep, topsep=0pt]
\itemsep-0.5em
 \item[$E_{\pi}$:]  is the expected value by following policy $\pi$
 \item[$S_{t}$:]  determines the state at a certain timestamp
\end{itemize}

\paragraph{Example} Let us reconsider the downhill example from figure \ref{fig:mdpDownhill} and define a simple policy, $\pi$, as follows. \\ \\
Since we aren't professional cyclists, we always decide to drive safely and if we have an accident on the track and land in the hospital we have a surgery to get back on the track as fast as possible in order to reach the goal.
This results in the policy listed in \eqref{eq:pi}.

\begin{equation}
  \begin{gathered}
        \pi :
          \begin{cases}
            S_{Start} \rightarrow A_{drive\_safely} \\
            S_{Hospital} \rightarrow A_{have\_surgery}
          \end{cases}
  \end{gathered}\label{eq:pi}
\end{equation}

Now we are able to apply the policy of our example and compute the state-value-function for the states.
The calculation is illustrated at \eqref{eq:sfunctionexample} and we set the discount factor $\gamma$ to 0.9.


\begin{equation}
  \begin{gathered}
   E_{\pi}(S_{Start})= 0.1 \cdot (-3) + 0.9 \cdot 3 = 2.4 \\
   E_{\pi}(S_{Hospital})= 1.0\cdot1.0 = 1.0 \\
   E_{\pi}(S_{Goal})= 0 \text{ , the value of a terminal state is always zero} \\
   V^{\pi}(S_{Start})= y^{0} \cdot E_{\pi}(S_{Start}) + y^{1} \cdot E_{\pi}(S_{Hospital}) + y^{2} \cdot E_{\pi}(S_{Goal}) \\
    = 0.9^0 \cdot 2.4 + 0.9^1 \cdot 1.0 + 0.9^2 \cdot 0 = 3.3 \\
  V^{\pi}(S_{Hospital})= y^{0} \cdot E_{\pi}(S_{Hospital}) y^{1} \cdot E_{\pi}(S_{Goal}) \\
    = 0.9^0 \cdot 1 + 0.9^1 \cdot 0 = 1.0
  \end{gathered}\label{eq:sfunctionexample}
\end{equation}


\subsubsection{Action-Value-Function}
The action-value-function expresses the expected value after taking an action $a$ from state $s$ and estimating how good it is to perform a certain action at a given state.
\begin{equation}
  \begin{gathered}
   Q^{\pi}(s,a)= E_{\pi} \bigg[ \sum_{k=0}^{\infty} {\gamma}^{k} R_{t+k+1} \mid S_{t} = s, A_{t} = a\bigg] \text{ , for all $s \in S$}
  \end{gathered}\label{eq:afunction}
\end{equation}
where:
\begin{itemize}[noitemsep, topsep=0pt]
\itemsep-0.5em
 \item[$E_{\pi}$:]  is the expected value by following policy $\pi$
 \item[$S_{t}$:]  determines the state at a certain timestamp
\end{itemize}

\paragraph{Example} For the action-value-function we change our policy, $\pi$, to the following. \\ \\
At the start of the track we flip a coin to choose our driving strategy and at the hospital, we act in the same way as our previous policy.
This leads us to the policy listed in \eqref{eq:pia}.
\begin{equation}
  \begin{gathered}
        \pi :
          \begin{cases}
            S_{Start} \rightarrow 0.5 \text{ probability for } A_{drive\_safely} \text{ and 0.5 probability for } A_{drive\_risky}  \\
            S_{Hospital} \rightarrow A_{have\_surgery}
          \end{cases}
  \end{gathered}\label{eq:pia}
\end{equation}

Now the action-value-function is able to answer the question of, "How good is it if you take the risky driving strategy in policy $\pi$?"

\begin{equation}
  \begin{gathered}
   E_{\pi}(S_{Start}, A_{drive\_risky})= 0.9 \cdot (-3) + 0.1 \cdot 10 = -1.7 \\
   E_{\pi}(S_{Hospital}, A_{have\_surgery})= 1.0\cdot1.0 = 1.0 \\
   E_{\pi}(S_{Goal})= 0 \text{ , the value of a terminal state is always zero} \\
   Q^{\pi}(S_{Start}, A_{drive\_risky})= y^{0} \cdot E_{\pi}(S_{Start}, A_{drive\_risky}) + y^{1} \cdot E_{\pi}(S_{Hospital}, A_{have\_surgery}) + y^{2} \cdot E_{\pi}(S_{Goal}) \\
   = 0.9^0 \cdot -1.7 + 0.9^1 \cdot 1.0 + 0.9^2 \cdot 0 = -0.8
   \end{gathered}
\end{equation}

\newpage
\section{Approaches}
The following section lists different approaches to handling reinforcement learning problems.
\subsection{Dynamic Programming}
Dynamic programming was first introduced by Richard Bellman \cite{bellman1954theory}.
The main idea is to simplify a complicated problem, break it into smaller subproblems and repeat that process recursively.
Examples of this are the Viterbi Algorithm \cite{forney1973viterbi} or Dijkstra's Algorithm \cite{dijkstra1959note} for the shortest path problem.
If the state-transition probabilities and the reward function are given, these algorithms are able to compute an optimal policy under the assumption that the model is provided perfectly.
In the worst case it takes polynomial time in the number of states and actions to find an optimality.
In reinforcement learning, we are unfortunately often faced with problems without a perfect model and without the computational power to apply such techniques.

In terms of reinforcement learning and dynamic programming the subproblems are defined as:

\begin{itemize}
    \item \textit{Policy evaluation} is a description of the process used to compute the state-value-function for an arbitrary policy. It helps us to quantify how powerful a policy is.
    \item \textit{Policy improvement} is the next step after evaluating a policy with the goal to make the policy better. For that purpose, we could use the action-value-function to take an other action at a certain state and determine if the new policy is an improvement.
    \item \textit{Policy iteration} combines policy evaluation with policy improvement. It also iterates over these processes to find an optimal policy. A downside of policy iteration could be that you have to evaluate a policy at every step.
    \item \textit{Value iteration} effectively combines, in each of its steps, one step of policy evaluation and one step of policy improvement.
\end{itemize}

\subsection{Monte Carlo methods}
In general a Monte Carlo method \cite{metropolis1949monte} describes any method that solves a problem by generating suitable random numbers and observes some properties based on them.
If we apply Monte Carlo methods to reinforcement learning, the agent learns directly from experience on complete episodes.
It is a model-free approach.
That means that the value-function is unknown.

For instance, we could consider or downhill ride problem from figure \ref{fig:mdpDownhill} and remove the state-transition probabilities.
Now we aren't able to determine the value-function.
However, we could follow an arbitrary policy and observe the states we reach dependent on our chosen actions.
If we repeat this process for enough time, we can estimate the value-function by the gained experience.
And in accordance to the law of large numbers it is even possible to find an optimal policy.

\subsection{Temporal Difference Learning}
Temporal difference learning is an enhancement of reinforcement learning that combines the Monte Carlo method and dynamic programming.
Similar to Monte Carlo methods it learns by experience without a model of the problem
and like dynamic programming it doesn't only adjust if the whole episode is finished.
Hence, it is able to update the estimated value-function after every action it takes.
Known temporal difference algorithms are \gls{Sarsa} and Q-learning.

Since Q-learning is essential to the taken approach of this term project, it is listed separately in section \ref{qlearning}.
\newpage

\section{Q-Learning} \label{qlearning}
Q-Learning is located in the area of model free reinforcement learning techniques \cite{watkins1989learning}.
The goal is to learn the action-value-function Q that represents the optimal policy.
The computation of Q as introduced by Watkins is listed in \eqref{eq:qvalues}.
If Q is known, you only have to select the action that gives the biggest reward for acting in an optimal manner.

\begin{equation}
  \begin{gathered}
   Q(S_{t},A_{t}) \leftarrow Q(S_{t},A_{t}) + \alpha[ R_{t} + \gamma \max\limits_{a} Q(S_{t+1},a) - Q(S_{t},A_{t})]
  \end{gathered}\label{eq:qvalues}
\end{equation}
where:
\begin{itemize}[noitemsep, topsep=0pt]
\itemsep-0.5em
 \item[Q:]  action-value-function
 \item[S:]  state
 \item[A:]  action
 \item[t:]  step
 \item[$\alpha$:]  (0, 1]
 \item[R:]  reward
 \item[$\gamma$:]  discount factor
 \item[a:]  all possible next states
\end{itemize}



\subsection{Example}
Since Q-learning is an essential part of the term project, we use an example to get more familiar with this technique.
The example is based on the Grid World game, which aims to get as fast as possible to a certain field from a random starting position.
It is illustrated in figure \ref{fig:gridworld}, and our particular version uses the following constraints: \label{gridworldexample}
\begin{itemize}
\itemsep0em
    \item 3 x 3 world size
    \item (0, 0) is the goal field
    \item allowed actions are left, right, up and down
    \item illegal actions are diagonal movements and leaving the grid
\end{itemize}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.28\textwidth,keepaspectratio]{images/gridworld.png}
        \caption{Grid World}
        \label{fig:gridworld}
\end{figure}



\subsubsection{Implementation} To illustrate how the Q-learning algorithm works, we will use an implementation in Python applied to the example of the Grid World game.

\begin{itemize}
    \item [1.] First, we define all the legal and illegal actions as you can see in listing \ref{lst:actions}.
\end{itemize}

\begin{lstlisting}[label=lst:actions, language=iPython, caption={action definition},captionpos=b]
def right(x, y):
    return x + 1 if x < 2 else x, y


def left(x, y):
    return x - 1 if x > 0 else x, y


def up(x, y):
    return x, y - 1 if y > 0 else y


def down(x, y):
    return x, y + 1 if y < 2 else y


actions = [left, right, up, down]

illegal_actions = [(0, 0, left), (0, 0, up), (1, 0, up), (2, 0, up),
                   (2, 0, right), (0, 1, left), (0, 2, left), (0, 2, down),
                   (1, 2, down), (2, 2, down), (2, 2, right), (2, 1, right)]
\end{lstlisting}

\begin{itemize}
    \item [2.] Second, we define the reward in relation to the state and the action. This is illustrated in \ref{lst:reward}. The goal state gets 100, illegal actions get -1 and the remaining are initialized with zeros.
\end{itemize}

\begin{lstlisting}[label=lst:reward, language=iPython, caption={reward definition},captionpos=b]
reward = {(0, 0, left): 100, (0, 0, right): 0, (0, 0, up): 100, (0, 0, down): 0,
          (0, 1, left): -1, (0, 1, right): 0, (0, 1, up): 100, (0, 1, down): 0,
          (0, 2, left): -1, (0, 2, right): 0, (0, 2, up): 0, (0, 2, down): -1,
          (1, 0, left): 100, (1, 0, right): 0, (1, 0, up): -1, (1, 0, down): 0,
          (1, 1, left): 0, (1, 1, right): 0, (1, 1, up): 0, (1, 1, down): 0,
          (1, 2, left): 0, (1, 2, right): 0, (1, 2, up): 0, (1, 2, down): -1,
          (2, 0, left): 0, (2, 0, right): -1, (2, 0, up): -1, (2, 0, down): 0,
          (2, 1, left): 0, (2, 1, right): -1, (2, 1, up): 0, (2, 1, down): 0,
          (2, 2, left): 0, (2, 2, right): -1, (2, 2, up): 0, (2, 2, down): -1, }
\end{lstlisting}


\begin{itemize}
    \item [3.] As you can see in listing \ref{lst:agent}, the agent initializes the Q values with zeros. It hosts a greedy strategy for the action choice and provides the train method.
\end{itemize}
\begin{lstlisting}[label=lst:agent, language=iPython, caption={the agent},captionpos=b, escapeinside={@}{@}]
class Agent:
    def __init__(self, gamma=0.95, exploration_rate=0.9):
        self.gamma = gamma
        self.exploration_rate = exploration_rate
        self.Q = {(0, 0, left): 0, (0, 0, right): 0, (0, 0, up): 0, (0, 0, down): 0,
                  (0, 1, left): 0, (0, 1, right): 0, (0, 1, up): 0, (0, 1, down): 0,
                  (0, 2, left): 0, (0, 2, right): 0, (0, 2, up): 0, (0, 2, down): 0,
                  (1, 0, left): 0, (1, 0, right): 0, (1, 0, up): 0, (1, 0, down): 0,
                  (1, 1, left): 0, (1, 1, right): 0, (1, 1, up): 0, (1, 1, down): 0,
                  (1, 2, left): 0, (1, 2, right): 0, (1, 2, up): 0, (1, 2, down): 0,
                  (2, 0, left): 0, (2, 0, right): 0, (2, 0, up): 0, (2, 0, down): 0,
                  (2, 1, left): 0, (2, 1, right): 0, (2, 1, up): 0, (2, 1, down): 0,
                  (2, 2, left): 0, (2, 2, right): 0, (2, 2, up): 0, (2, 2, down): 0, }

    def max_next_q_value(self, x, y):
        next_states = [action(x, y) for action in actions]
        all_qs = []
        for action in actions:
            all_qs += [self.Q[(state[0], state[1], action)] for state in next_states]
        return max(all_qs)

    def choose_greedy_action(self, x, y): @\label{line:greedy}@
        q_values_actions = [(self.Q[x, y, action], action) for action in actions
                            if (x, y, action) not in illegal_actions]
        return choice([action for q_value, action in q_values_actions
                       if max(q_values_actions, key=lambda z: z[0])[0] == q_value])

    def train(self, episodes):
        for _ in range(episodes):
            x, y = randint(0, 2), randint(0, 2)
            while not (x == 0 and y == 0):
                action = choice(actions) if uniform(0, 1) >= self.exploration_rate \
                                         else self.choose_greedy_action(x, y) @\label{line:exploration}@
                next_x, next_y = action(x, y)
                self.Q[(x, y, action)] = reward[x, y, action] + \
                                     self.gamma * self.max_next_q_value(next_x, next_y) @\label{line:qvaluefunction}@
                x, y = next_x, next_y

    def run(self, x, y):
        action_counter = 0
        while not (x == 0 and y == 0):
            action = self.choose_greedy_action(x, y)
            x, y = action(x, y)
            action_counter += 1
        return action_counter
\end{lstlisting}

The agent is the core component of the algorithm and uses the train method to optimize the action-value-function, Q.
The update of the action-value-function uses the equation listed in \eqref{eq:qvalues} with $\alpha = 1$ (listing \ref{lst:agent}, line \ref{line:qvaluefunction}).
It is important to mention that the choice of the next action is implemented in a greedy way (listing \ref{lst:agent}, line \ref{line:greedy}).
Further, there is an exploration mechanism added (listing \ref{lst:agent}, line \ref{line:exploration}) to make sure that all the fields are visited
and you doesn't end in a local minima.


\begin{itemize}
    \item [3.] Finally, we are able to train and run the agent to verify our result. See \ref{lst:train}.
\end{itemize}

\begin{lstlisting}[label=lst:train, language=iPython, caption={train and run the agent},captionpos=b]
agent = Agent()
agent.train(episodes=1000)

for start_x, start_y in [(2, 2), (2, 1), (1, 1)]:
        print('Start:({},{}) It took {} actions to the goal.'
              .format(start_x, start_y, agent.run(start_x, start_y)))

'''output
Start:(2,2) It took 4 actions to the goal.
Start:(2,1) It took 3 actions to the goal.
Start:(1,1) It took 2 actions to the goal.
'''
\end{lstlisting}

\subsubsection{Result}
As you can see from the output in listing \ref{lst:train}, the agent took a shortest path to the goal field by using the trained action-value-function and a greedy choice of the next action.
The visualization of the Q values is illustrated in \ref{tab:Q}.
If you start at any field in the grid and choose the highest action-value, you will find a shortest path through the world.

\begin{table}[H]
\centering
\begin{tabular}{ |p{2.5cm}|p{2.5cm}|p{2.5cm}|}
\hline
(0,0)
\begin{itemize}
\itemsep-0.5em
    \item[$\leftarrow$:] 0.0
    \item[$\rightarrow$:] 0.0
    \item[$\uparrow$:] 0.0
    \item[$\downarrow$:] 0.0
\end{itemize}
&
(1,0)
\begin{itemize}
\itemsep-0.5em
    \item[$\leftarrow$:] 2000.0
    \item[$\rightarrow$:] 1900.0
    \item[$\uparrow$:] 1899.0
    \item[$\downarrow$:] 1900.0
\end{itemize}
&
(2,0)
\begin{itemize}
\itemsep-0.5em
    \item[$\leftarrow$:] 1900.0
    \item[$\rightarrow$:] 1899.0
    \item[$\uparrow$:] 1899.0
    \item[$\downarrow$:] 1805.0
\end{itemize}
\\ \hline
(0,1)
\begin{itemize}
\itemsep-0.5em
    \item[$\leftarrow$:] 1899.0
    \item[$\rightarrow$:] 1900.0
    \item[$\uparrow$:] 2000.0
    \item[$\downarrow$:] 1900.0
\end{itemize}
&
(1,1)
\begin{itemize}
\itemsep-0.5em
    \item[$\leftarrow$:] 1900.0
    \item[$\rightarrow$:] 1805.0
    \item[$\uparrow$:] 1900.0
    \item[$\downarrow$:] 1805.0
\end{itemize}
&
(2,1)
\begin{itemize}
\itemsep-0.5em
    \item[$\leftarrow$:] 1900.0
    \item[$\rightarrow$:] 1804.0
    \item[$\uparrow$:] 1900.0
    \item[$\downarrow$:] 1805.0
\end{itemize}
\\ \hline
(0,2)
\begin{itemize}
\itemsep-0.5em
    \item[$\leftarrow$:] 1899.0
    \item[$\rightarrow$:] 1805.0
    \item[$\uparrow$:] 1900.0
    \item[$\downarrow$:] 1899.0
\end{itemize}
&
(1,2)
\begin{itemize}
\itemsep-0.5em
    \item[$\leftarrow$:] 1900.0
    \item[$\rightarrow$:] 1805.0
    \item[$\uparrow$:] 1900.0
    \item[$\downarrow$:] 1804.0
\end{itemize}
&
(2,2)
\begin{itemize}
\itemsep-0.5em
    \item[$\leftarrow$:] 1805.0
    \item[$\rightarrow$:] 1804.0
    \item[$\uparrow$:] 1805.0
    \item[$\downarrow$:] 1804.0
\end{itemize}
\\ \hline
\end{tabular}
\caption{values of the action-value-function, Q}
\label{tab:Q}
\end{table}

\newpage
\section{Deep Q-Learning}
Most of the problems aren't as small as our Grid World game from \ref{gridworldexample}
and it is often impossible to compute or traverse the whole state space in an affordable amount of time.
Hence, we need techniques that are good in generalization and are able to handle problems in a variety of forms.
And this is exactly where deep learning meets reinforcement learning.
Deep learning does a great job in generalization and in handling unknown situations.
In particular, the neural networks represent and learn to depict the action-value-function.
This enabled great successes in games like \gls{Go} by DeepMind \cite{mnih2015human}.

\subsection{Functionality}
Deep Q-Learning roughly works as follows:
\begin{enumerate}
    \item First it randomly initializes our neural network, which represents our action-value-function.
    \item Then we can simulate some games and memorize them.
    \item After that, we are able to train the neural network based on the memorized games. This process is called experience replay.
    \item Finally, we repeat this process until we are pleased with the behavior of the neural network or a certain number of episodes has been completed.
\end{enumerate}

The full algorithm introduced by V. Mnih et al. \cite{mnih2015human} is listed in \ref{alg:dql}.

\begin{algorithm}[H]
\begin{algorithmic}
\State Initialize replay memory $D$ to size $N$
\State Initialize action-value-function $Q$ with random weights $\theta$
\State Initialize target action-value-function $\hat{Q}$ with random weights $\theta^{-}=\theta$
\For{episode = 1, M}
	\State Initialize state $s_{1} = \{x_1\}$ and preprocess sequence $\phi_1=\phi(s_1)$
	\For{ t = 1, T}
		\State With probability $\varepsilon$ select random action $a_{t}$
		\State Otherwise select $a_{t}=argmax_{a}  Q(\phi(s_t),a; \theta)$
		\State Execute action $a_{t}$ in emulator and observe $r_{t}$ and image $x_{t+1}$
		\State Set $s_{t+1} = s_t,a_t,x_{t+1}$ and process $\phi_{t+1}=\phi(s_{t+1})$
		\State Store transition $(\phi,a_{t},r_{t},\phi_{t+1})$ in D
		\State Sample a minibatch of transitions $(s_{j},a_{j},r_{j},s_{j+1})$ from $D$
		\[ \text{Set } y_{j}:=
          \begin{cases}
            r_{j} & \text{if episode terminates at step } j+1 \\
            r_{j} + \gamma \cdot max_{a'}  \hat{Q}(\phi_{j+1},a'; \theta^{-}) & \text{otherwise}
          \end{cases}
        \]
		\State Perform a gradient step on $(y_{j}-Q(\phi_{j},a_{j}; \theta))^2$ with respect to $\theta$
	    \State Every $C$ steps reset $\hat{Q} = Q$
	\EndFor
\EndFor
\end{algorithmic}
\caption{Deep Q Learning with experience replay}
\label{alg:dql}
\end{algorithm}


Now, I'd like to point out some of the details of the algorithm.


\subsubsection{Experience replay}
To train the neural network, we first store the current state, the taken action, the gained reward and the next state for all the steps of the game.
After a lot of simulated games, we train the model based on random mini-batches of the memorized experiences.

\subsubsection{Two neural networks}
As you might have noticed, in the algorithm there are two neural network used.
One is updated during the experience replay and the other does the predictions for the taken actions.
When a certain amount of mini-batches are trained, the decision network will be updated.
The reason for this process is that it helps to stabilize the learning of the non-linear function.

\subsubsection{Loss function}
As you can see in equation \eqref{eq:lossfunction}, deep Q-learning often uses a mean squared error loss.
\begin{equation}
  \begin{gathered}
  loss = (r + \gamma \cdot max_{a'}  \hat{Q}(s', a') - Q(s,a))^{2}
  \end{gathered}\label{eq:lossfunction}
\end{equation}
where:
\begin{itemize}[noitemsep, topsep=0pt]
\itemsep-0.5em
 \item[r:]  reward
 \item[$\gamma$:]  discount factor
 \item[a':]  next action
 \item[a:]  action action
 \item[s':]  next state
 \item[s:]  state
 \item[$\hat{Q}$:]  target network
 \item[Q:]  prediction network
\end{itemize}

If you are using a library like Keras \cite{keras}, you only have to provide the state and the target value as input for the neural network
and set the \gls{loss function} to the mean squared error.

\subsubsection{Exploration}
To reduce the problem of getting stuck in a local minima, we take a random action with a certain possibility.
In algorithm \ref{alg:dql}, this is shown with probability $\varepsilon$ to select a random action $a_{t}$.


\subsection{Example}
To get a better understanding of deep Q-learning, let's reconsider our Grid World example from \ref{gridworldexample} and change the Q-leaning action-value-function to a deep Q-learning action-value-function.

\subsubsection{Implementation}

\begin{itemize}
    \item [1.] Since neural networks work on \gls{NumPy}, we need a helper function that brings the coordinates into the right shape. This is shown in listing \ref{lst:helper}.
\end{itemize}

\begin{lstlisting}[label=lst:helper, language=iPython, caption={position to state},captionpos=b]
def position_to_state(x, y):
    state = np.zeros(16, dtype='float32')
    state[4 * y + x] = 1.
    return np.expand_dims(state, axis=0)
\end{lstlisting}


\begin{itemize}
    \item [2.] The agent stores played games to train the neural network. Further, it implements an act method that recommends actions based on the decisions of the neural network and provides the related neural network model. See in listing \ref{lst:rlagent}.
\end{itemize}

\begin{lstlisting}[label=lst:rlagent, language=iPython, caption={reinforcement learning agent},captionpos=b]
class Agent:
    def __init__(self, gamma=0.95, exploration_rate=0.9):
        self.gamma = gamma
        self.exploration_rate = exploration_rate
        self.memory = deque([], maxlen=1000)
        self.model = self.get_model()
        self.target_model = self.get_model()

    def get_model(self):
        model = Sequential()
        model.add(Dense(24, input_shape=(16,), activation='relu'))
        model.add(Dense(24, activation='relu'))
        model.add(Dense(4, activation='linear'))
        model.compile(loss='mse', optimizer=Adam(lr=0.01))
        return model

    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    def replay(self):
        batch = sample(self.memory, 100) if len(self.memory) > 100 else self.memory
        for state, action, r, next_state, done in batch:
            target = r
            if not done:
                state = position_to_state(state[0], state[1])
                next_state = position_to_state(next_state[0], next_state[1])
                target = r + self.gamma \
                             * np.amax(self.target_model.predict(next_state)[0])
            target_values = self.model.predict(state)
            target_values[0][actions.index(action)] = target
            self.model.fit(state, target_values, epochs=1, verbose=0)
        self.target_model.set_weights(self.model.get_weights())

    def act(self, x, y):
        state = position_to_state(x, y)
        if np.random.rand() >= self.exploration_rate: @\label{line:exploration}@
            return choice(actions)
        act_values = self.model.predict(state)
        return actions[np.argmax(act_values[0])]

    def train(self, episodes=1):
        for _ in range(episodes):
            x, y = randint(0, 2), randint(0, 2)
            while not (x == 0 and y == 0):
                action = self.act(x, y)
                next_x, next_y = action(x, y)
                done = False if not (x == 0 and y == 0) else True
                self.remember(state=(x, y), action=action, reward=reward[x, y, action],
                              next_state=(next_x, next_y), done=done)
                x, y = next_x, next_y
            self.replay()

    def run(self, x, y):
        action_counter = 0
        self.exploration_rate = 1.1
        while not (x == 0 and y == 0):
            action = self.act(x, y)
            x, y = action(x, y)
            action_counter += 1
        return action_counter
\end{lstlisting}

Important corner points to mention about the agent are the fact that it uses two neural networks, a model to train after every decision and a sporadically updated target model.
Further, there is the act method that that takes with the probability of the exploration rate a random action or lets the neural network decide(listing \ref{lst:rlagent}, line \ref{line:exploration}).


\begin{itemize}
    \item [2.] To verify the result, we can train and run the agent. See listing \ref{lst:runtrain}.
\end{itemize}

\begin{lstlisting}[label=lst:runtrain, language=iPython, caption={train agent},captionpos=b]
agent = Agent()
agent.train(episodes=1000)

for start_x, start_y in [(2, 2), (2, 1), (1, 1)]:
    print('Start:({},{}) It took {} actions to the goal.'
          .format(start_x, start_y, agent.run(start_x, start_y)))

'''
Start:(2,2) It took 4 actions to the goal.
Start:(2,1) It took 3 actions to the goal.
Start:(1,1) It took 2 actions to the goal.
'''
\end{lstlisting}

As you can see in listing \ref{lst:runtrain}, after the training phase, the agent takes actions that led to an optimal path through the Grid World.

\newpage
\subsubsection{Result}
To visualize the output, we let the neural network predict all the fields and plotted the values for all the possible actions.
The result is listed in table \ref{tab:DQ}.
As you can see, if you follow a greedy strategy considering the values of the table, you end up with a shortest path through the Grid World.
\begin{table}[H]
\centering
\begin{tabular}{ |p{2.5cm}|p{2.5cm}|p{2.5cm}|}
\hline
(0,0)
\begin{itemize}
\itemsep-0.5em
    \item[$\leftarrow$:] 355.4
    \item[$\rightarrow$:] 329.5
    \item[$\uparrow$:] 359.6
    \item[$\downarrow$:] 328.5
\end{itemize}
&
(1,0)
\begin{itemize}
\itemsep-0.5em
    \item[$\leftarrow$:] 438.8
    \item[$\rightarrow$:] 402.5
    \item[$\uparrow$:] 420.4
    \item[$\downarrow$:] 398.5
\end{itemize}
&
(2,0)
\begin{itemize}
\itemsep-0.5em
    \item[$\leftarrow$:] 421.2
    \item[$\rightarrow$:] 388.0
    \item[$\uparrow$:] 406.7
    \item[$\downarrow$:] 384.6
\end{itemize}
\\ \hline
(0,1)
\begin{itemize}
\itemsep-0.5em
    \item[$\leftarrow$:] 406.5
    \item[$\rightarrow$:] 385.5
    \item[$\uparrow$:] 438.5
    \item[$\downarrow$:] 389.1
\end{itemize}
&
(1,1)
\begin{itemize}
\itemsep-0.5em
    \item[$\leftarrow$:] 416.8
    \item[$\rightarrow$:] 381.9
    \item[$\uparrow$:] 420.2
    \item[$\downarrow$:] 378.1
\end{itemize}
&
(2,1)
\begin{itemize}
\itemsep-0.5em
    \item[$\leftarrow$:] 395.7
    \item[$\rightarrow$:] 365.4
    \item[$\uparrow$:] 400.9
    \item[$\downarrow$:] 363.3
\end{itemize}
\\ \hline
(0,2)
\begin{itemize}
\itemsep-0.5em
    \item[$\leftarrow$:] 389.6
    \item[$\rightarrow$:] 369.7
    \item[$\uparrow$:] 418.7
    \item[$\downarrow$:] 373.5
\end{itemize}
&
(1,2)
\begin{itemize}
\itemsep-0.5em
    \item[$\leftarrow$:] 392.6
    \item[$\rightarrow$:] 363.7
    \item[$\uparrow$:] 398.4
    \item[$\downarrow$:] 362.3
\end{itemize}
&
(2,2)
\begin{itemize}
\itemsep-0.5em
    \item[$\leftarrow$:] 375.4
    \item[$\rightarrow$:] 352.9
    \item[$\uparrow$:] 384.5
    \item[$\downarrow$:] 354.4
\end{itemize}
\\ \hline
\end{tabular}
\caption{values of the neural network}
\label{tab:DQ}
\end{table}

\newpage
