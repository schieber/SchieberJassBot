\chapter{Environments}
To apply reinforcement learning techniques on a game, you need to have an environment for the game and some kind of an \gls{API} to get all the necessary information.
As already mentioned, Zühlke provide a Schieber \gls{Jass} server.
The backend of the server is based on Node.js and the frontend uses React.
It is possible to register multiple players and interact with the server over \gls{WebSockets}.
Since reinforcement learning is a highly computing intensive task and you have to simulate a huge amount of games,
I decided to build a Schieber Jass environment on my own to get faster feedback from the learning process.
This allowed me to be able to test more different neural network model configurations and hyperparameters.
Besides the technical advantages, it was a good starting point to refresh the domain knowledge and the environment brought a lot of flexibility concerning the API.
In contrast to the original Schieber \gls{Jass} game, the Zühlke \gls{Jass} server and pyschieber don't implement the \gls{Wies} process.

In addition to the Schieber Jass environment, I developed a Tic-Tac-Toe and a minified Jass version to get familiar with reinforcement learning techniques and to verify the achieved results.
The following sections describe these environments.

\section{pyschieber} \label{pyschieber}

\begin{tcolorbox}[colback=Cerulean!5!white,colframe=Cerulean!75!black, title={Repository},]
\begin{tabular}{ p{2.5cm} l }
  Name: & pyschieber \\
  URL: & \url{https://github.com/murthy10/pyschieber} \\
  PyPI: & \url{https://pypi.org/project/pyschieber/} \\
  Reference: & \cite{pyschieber} \\
\end{tabular}
\end{tcolorbox}

pyschieber is the name of my own developed Schieber Jass environment.
It is publicly available and offers a simple \gls{CLI} to play and test the Schieber Jass game.
The source code is all written in Python due to the fact that a lot of machine learning libraries provide a Python interface.
In particular Keras \cite{keras} and Tensorflow \cite{tensorflow}, which are the machine learning libraries the project used, offer a Python \gls{API}.
Thus, it is possible to directly interact respectively integrate your own bots into the game environment without the overhead of any network requests.


\paragraph{Performance comparison} To test if there is a performance gain between pyschieber and the Zühlke Challenge server I simulated 1000 tournaments with a point limit of 2500 and 4 random players against each other in both environments.
The result of this performance test is listed in table \ref{tab:performance_test}.
As you can see, the pyschieber environment resulted in a performance gain of about 9.8 times.

\begin{table}[H]
\centering
\begin{tabular}{| p{5.5cm} | l | }
\hline
\rowcolor{lightblue}
Application & Duration \\
\hline
    Zühlke Challenge server & 1674.5\SI[]{}{\second} (27\SI[]{}{\minute} 54.5\SI[]{}{\second}) \\ \hline
    pyschieber & 171.2\SI[]{}{\second}  (2\SI[]{}{\minute} s 51.2\SI[]{}{\second} )\\
\hline
\end{tabular}
\caption{environments performance test}
\label{tab:performance_test}
\end{table}

\subsection{Description}
pyschieber essentially consists of two main parts.
On the one hand, there is the game environment that is responsible for the gameplay, the distribution of the cards, the checking of the rules and providing all the relevant information for the players.
On the other hand, there is the player who is able to participate in the game.
The player is called by the environment whenever he has to choose a \gls{Trumpf} or play a card.
At those moments and additionally when a \gls{Stich} is over he gets information about the game state.

\subsubsection{Installation}
In addition to the \gls{GitHub} repository, pyschieber is available on \gls{PyPI} \cite{pypi}.
Hence, the simplest way to install pyschieber is to use \gls{pip}. See listing \ref{lst:pip_pyschieber}.

\begin{lstlisting}[language=bash, backgroundcolor = \color{mygrey},caption={pyschieber installation},captionpos=b, label={lst:pip_pyschieber}]
  $ pip install pyschieber
\end{lstlisting}

\subsubsection{Usage}
After the installation, you can import all the pyschieber dependencies as usual into your own Python project.
Furthermore, you can run the pyschieber command on your console, get an impression of the \gls{CLI} and play against a seleciton of players.


To get an initial feeling for the pyschieber playground let's have a look at an example.

\begin{itemize}
    \item [1.] The first thing you have to do is to instantiate a new Tournament class and define the point limit. In our case we chose 1500 as illustrated in listing \ref{tournament}.
\end{itemize}
\begin{lstlisting}[label=tournament, language=iPython, caption={instantiate a new Tournament},captionpos=b]
from pyschieber.tournament import Tournament

tournament = Tournament(point_limit=1500)
\end{lstlisting}

\begin{itemize}
    \item [2.] Next, add the players to your tournament. In our example we used the erratic RandomPlayers Tick, Trick, Track and the GreedyPlayer Dagobert. You can see this in listing \ref{addPlayyer}.
\end{itemize}
\begin{lstlisting}[label=addPlayyer, language=iPython, caption={add players to the tournament},captionpos=b]
from pyschieber.player.random_player import RandomPlayer
from pyschieber.player.greedy_player.greedy_player import GreedyPlayer

players = [RandomPlayer(name='Tick'), RandomPlayer(name='Trick'),
           RandomPlayer(name='Track'), GreedyPlayer(name='Dagobert')]

[tournament.register_player(player) for player in players]
\end{lstlisting}

\begin{itemize}
    \item [3.] Now we are ready to play. How to start the game is displayed in listing \ref{starttournament}
\end{itemize}
\begin{lstlisting}[label=starttournament, language=iPython, caption={start the tournament},captionpos=b]
tournament.play()
\end{lstlisting}


As you might have noticed, we registered two different types of players in our tournament.
Hence, the idea is to use the players as an access point to the game and to build your own player.


\paragraph{Methods} The recommended way to realize your own player is to inherit from the BasePlayer class.
Due to the fact that Python uses duck typing, the inheritance is not absolutely necessary.
The minimum your player has to provide are the methods listed in table \ref{tab:providedmethods}.

\begin{table}[H]
\centering
\begin{tabular}{| p{4.5cm} | p{9.5cm} |}
\hline
\rowcolor{lightblue}

Method & Description \\
\hline
    set\_card(card) & set\_card is called multiple times at the beginning of every round by the dealer. The cards you get represent the cards on your hand, which you have to play during the game. \\ \hline
    choose\_trumpf(geschoben) & This method is called when it's your turn to choose the \gls{Trumpf}. The parameter geschoben is True if your teammate passes the decision to you. The type of your response has to be a generator. \\ \hline
    choose\_card(state) & The environment calls this method if it's your turn to choose a card from your hand. The response has to be a generator as well and is recalled until the chosen card is allowed. \\ \hline
    stich\_over(state) & After every \gls{Stich} all the players get information about the current game state. \\
\hline
\end{tabular}
\caption{methods you have to provide}
\label{tab:providedmethods}
\end{table}

\newpage
\paragraph{State} As you might have noticed, the methods choose\_card and stich\_over have a parameter called state.
It represents the whole game state in the form of a Python dictionary and provides all the relevant information to take appropriate card choosing decisions.
The information served is the \gls{Trumpf}, if it is geschoben or not, the point limit, the points of the teams, all the Stiche with the winning player ID, the chosen cards and the current cards on the table in the sequence they are played.
Listing \ref{lst:gamestate} shows an example of the state at the moment when player 3 has to choose a card and one \gls{Stich} has already been played.

\begin{lstlisting}[label=lst:gamestate, language=iPython, caption={game state example},captionpos=b]
{
   "trumpf":"OBE_ABE",
   "geschoben":false,
   "point_limit":1500,
   "teams":[
      {
         "points":72
      },
      {
         "points":73
      }
   ],
   "stiche":[
      {
         "player_id":0,
         "played_cards":[
            {
               "player_id":0,
               "card":"<BELL:Koennig>"
            },
            {
               "player_id":1,
               "card":"<BELL:Banner>"
            },
            {
               "player_id":2,
               "card":"<BELL:6>"
            },
            {
               "player_id":3,
               "card":"<BELL:9>"
            }
         ],
         "trumpf":"OBE_ABE"
      }
   ],
   "table":[
      {
         "player_id":0,
         "card":"<ACORN:8>"
      },
      {
         "player_id":1,
         "card":"<SHIELD:6>"
      },
      {
         "player_id":2,
         "card":"<ACORN:7>"
      }
   ]
}
\end{lstlisting}

\newpage
\paragraph{Example player} To get more familiar with this concept, let's have a look at the implementation of the already mentioned RandomPlayer, illustrated in listing \ref{randomplayer}.

\begin{lstlisting}[label=randomplayer, language=iPython, caption={pyschieber player implementation example},captionpos=b]
import random

from pyschieber.player.base_player import BasePlayer
from pyschieber.trumpf import Trumpf


class RandomPlayer(BasePlayer):
    def choose_trumpf(self, geschoben):
        return move(choices=list(Trumpf))

    def choose_card(self):
        return move(choices=self.cards)


def move(choices):
    allowed = False
    while not allowed:
        choice = random.choice(choices)
        allowed = yield choice
        if allowed:
            yield None

\end{lstlisting}

Listing \ref{randomplayer} shows a basic example of a player implementation that inherits from the BasePlayer.
The strategy of this player is to randomly choose a \gls{Trumpf} or a card until the game environment accepts the input.
It is not the most sophisticated way to play Schieber Jass, nevertheless it demonstrates a working player that can interact with the environment by providing the methods choose\_trumpf(geschoben) and stich\_over(state).

Now you should be ready to implement your own player and beat the random players Tick, Trick and Track.
For more information, have a look at the \gls{GitHub} repository \cite{pyschieber}.

\paragraph{Zühlke Schieber Jass server}
To use the beautiful user interface of the Zühlke Jass Server pyschieber provides a wrapper class.
It enables the integration of pyschieber players into the Schieber Jass server, parses the game states and takes care of the network communication.
An example is illustrated in listing \ref{serverplayer}.

\begin{lstlisting}[label=serverplayer, language=iPython, caption={example integration of a greedy player},captionpos=b]
from pyschieber.player.greedy_player.greedy_player import GreedyPlayer
from pyschieber.player.server_player.server_player import ServerPlayer

address="ws://127.0.0.1:3000"
session_name="test"
greedy_player = GreedyPlayer(name='Greedy player')

server_player = ServerPlayer(pyschieber_bot=greedy_player, server_address=address,
                             session_name=session_name)
\end{lstlisting}


\newpage
\section{pytictactoe} \label{pytictactoe}

\begin{tcolorbox}[colback=Cerulean!5!white,colframe=Cerulean!75!black, title={Repository},]
\begin{tabular}{ p{2.5cm} l }
  Name: & pytictactoe \\
  URL: & \url{https://github.com/murthy10/pytictactoe} \\
  Reference: & \cite{pytictactoe} \\
\end{tabular}
\end{tcolorbox}

Since the efforts in using reinforcement learning techniques applied in the Schieber Jass game weren't successful at the beginning of the term project,
I decided to go a step back and try a more simple game, Tic-Tac-Toe.
Hence, I implemented a Tic-Tac-Toe environment with an \gls{API} similar to pyschieber, some opponent players and a basic \gls{CLI}.
The source code is publicly available on \gls{GitHub}.

\subsection{Description}

\subsubsection{Installation}
To install pytictactoe you have to clone the repository, switch into the new directory, install the requirements and execute the setup.py. The commands are listed in \ref{lst:installpytictactoe}.

\begin{lstlisting}[language=bash, backgroundcolor = \color{mygrey},caption={pytictactoe installation},captionpos=b, label={lst:installpytictactoe}]
  $ git clone https://github.com/Murthy10/pytictactoe.git
  $ cd pytictactoe
  $ pip install -r requirements.txt
  $ python setup.py install
\end{lstlisting}

\subsubsection{Usage}
After the installation, you are ready to build your own player and use the \gls{CLI}.
A player can be implemented in a similar way to the players of pyschieber.
To get familiar with the player implementation, let's have a look at the example listed in \ref{tictactoeplayer}.

\begin{lstlisting}[label=tictactoeplayer, language=iPython, caption={Tic-Tac-Toe player implementation example},captionpos=b]
import random

from pytictactoe.field import Field
from pytictactoe.player.base_player import BasePlayer


class RandomPlayer(BasePlayer):
    def choose_field(self, grid):
        allowed = False
        while not allowed:
            field = Field(x=random.randint(0, 2), y=random.randint(0, 2))
            allowed = yield field
            if allowed:
                yield None
\end{lstlisting}

The RandomPlayer as you can see in \ref{tictactoeplayer} is pretty naive and he simply chooses a position randomly.
If the choice is not allowed, he randomly chooses a new one until the rules of Tic-Tac-Toe are satisfied.
A pytictactoe player needs to provide the choose\_field(grid) method.

\paragraph{CLI} In addition to the \gls{API}, pytictactoe provides a command-line interface to test your players. An example is illustrated in figure \ref{fig:pytictactoe}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth,keepaspectratio]{images/tictactoe.png}
        \caption{\gls{CLI} pytictactoe}
        \label{fig:pytictactoe}
\end{figure}

\newpage
\section{pyMiniJass} \label{pyMiniJass}

\begin{tcolorbox}[colback=Cerulean!5!white,colframe=Cerulean!75!black, title={Repository},]
\begin{tabular}{ p{2.5cm} l }
  Name: & pyMiniJass \\
  URL: & \url{https://github.com/murthy10/pyMiniJass} \\
  Reference: & \cite{pyMiniJass} \\
\end{tabular}
\end{tcolorbox}

To have similar behavior as the Schieber Jass game has, but less complexity, I developed a minified version of the game.
The important similarities are:

\begin{itemize}
    \item Multiplayer game
    \item Having a teammate
    \item Unallowed moves (Cards that are not allowed to play)
\end{itemize}

This results in a game with the following behavior.

\subsection{Game description}

\subsubsection{Structure}
The pyMiniJass is structured as follows:
\begin{itemize}
    \item Two types of cards, named as A and B
    \item 8 cards per type, resulting in a total of 16
    \item Four players, two of them building a team
    \item A game has four rounds
    \item The highest card with the same type of the starting card wins the round
\end{itemize}

\subsubsection{Gameplay}
First the four players have to build two teams of two members each.
From the perspective of every player the order of card choosing is defined as follows.
If you start the round, the next player is your first opponent.
After that your teammate has to choose the card and finally your second opponent plays the last card of the round.
At the beginning of the game, every player randomly gets four cards.
After the card dealing phase, a random player starts to choose a card.
Now, each player after another has to choose a card of their own ones.
If available, the chosen card has to be of the same type as the first of the round.
When every player has played a card, the highest card of the same type as the starting card wins the round.
The team of the winning player gets as many points as the values of the cards have.
The next round starts with the winner of the last one.
This procedure is repeated until no cards are left, leading to a total of four rounds.
At the end of the game, the team with the most points wins.


\subsection{Description}
\subsubsection{Installation}
To install pyMiniJass you have to clone the repository, switch into the new directory, install the requirements and execute the setup.py. The commands are listed in \ref{lst:installpytictactoe}.

\begin{lstlisting}[language=bash, backgroundcolor = \color{mygrey},caption={pyMiniJass installation},captionpos=b, label={lst:installpytictactoe}]
  $ git clone https://github.com/Murthy10/pyMiniJass.git
  $ cd pyMiniJass
  $ pip install -r requirements.txt
  $ python setup.py install
\end{lstlisting}

\subsubsection{Usage}
After the installation, you are ready to build your own player. The \gls{API} is similar to that of the pyschieber mentioned in section \ref{pyschieber}.

\paragraph{Own player} The recommended way to build a player is to inherit from the provided BasePlayer and implement the following methods:
\begin{itemize}
    \item choose\_card(table=None)
    \item round\_over(round=None)
\end{itemize}

\paragraph{CLI} In addition to the \gls{API}, pyMiniJass provides a command-line interface to test your players. An example turn is represented in figure \ref{fig:pyminijass}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth,keepaspectratio]{images/pyminijass.png}
        \caption{\gls{CLI} pyMiniJass}
        \label{fig:pyminijass}
\end{figure}
