import pytest
from timeit import default_timer as timer
from multiprocessing import Process
from pyschieber.player.random_player import RandomPlayer

from schieberjassbot.jass_challenge_wrapper.jass_challenge_bot import JassChallengeBot


@pytest.mark.statistical
def test_challenge_performance():
    server_address = "ws://127.0.0.1:3000"
    session_name = "test"
    number_of_tournaments = 100
    players = [
        JassChallengeBot(pyschieber_bot=RandomPlayer(name='RandomPlayer1'), server_address=server_address,
                         chosen_team_index=1, session_name=session_name, max_games=number_of_tournaments),
        JassChallengeBot(pyschieber_bot=RandomPlayer(name='RandomPlayer2'), server_address=server_address,
                         chosen_team_index=1, session_name=session_name, max_games=number_of_tournaments),
        JassChallengeBot(pyschieber_bot=RandomPlayer(name='RandomPlayer3'), server_address=server_address,
                         chosen_team_index=0, session_name=session_name, max_games=number_of_tournaments),
        JassChallengeBot(pyschieber_bot=RandomPlayer(name='RandomPlayer4'), server_address=server_address,
                        chosen_team_index=0, session_name=session_name, max_games=number_of_tournaments)
    ]
    jobs = []
    start = timer()
    for player in players:
        process = Process(target=player.start)
        jobs.append(process)
        process.start()

    for job in jobs:
        job.join()

    end = timer()
    print("\nTo run {0} tournaments it took {1:.2f} seconds.".format(number_of_tournaments, end - start))