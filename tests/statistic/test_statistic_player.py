from timeit import default_timer as timer

from pyschieber.player.random_player import RandomPlayer
from pyschieber.tournament import Tournament

from schieberjassbot.statistic.statistic_player import StatisticPlayer


def test_statistc_player():
    start = timer()

    rounds = 100000
    tournament = Tournament()
    statistic_player = StatisticPlayer()
    players = [statistic_player, RandomPlayer(), RandomPlayer(), RandomPlayer()]
    [tournament.register_player(player) for player in players]
    tournament.play(rounds=rounds, use_counting_factor=False)

    end = timer()

    statistic_player.print_statistic()
    print("\nTo run {0} rounds it took {1:.2f} seconds.".format(rounds, end - start))

    assert True
