from setuptools import setup, find_packages

with open('README.md') as f:
    readme = f.read()

    setup(
        name='SchieberJassBot',
        version='0.1.0',
        description='Reinforcement Scheiber Jass Bot',
        long_description=readme,
        author='Samuel Kurath',
        author_email='samuel.kurath@gmail.com',
        url='https://gitlab.com/schieber/SchieberJassBot',
        license='MIT',
        packages=find_packages(exclude=('tests', 'doc', 'docker')),
    )
