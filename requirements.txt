pyschieber>=0.9.5
Keras==2.1.4
h5py==2.7.1
matplotlib==2.2.0
websockets==4.0.1
