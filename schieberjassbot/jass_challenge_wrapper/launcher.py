import os
import argparse
import logging
from multiprocessing import Process

from pyschieber.example.greedy_player import GreedyPlayer

from schieberjassbot.player.rl_player import RLPlayer
from schieberjassbot.jass_challenge_wrapper.jass_challenge_bot import JassChallengeBot

logging.basicConfig(level=logging.INFO, format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s', )


def launch(server_address, session_name):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    model_path = dir_path + '/../../tests/weights/rl1_model.h5'
    opponents = [
        JassChallengeBot(pyschieber_bot=GreedyPlayer(name='Greedy1'), server_address=server_address,
                         chosen_team_index=1, session_name=session_name),
        JassChallengeBot(pyschieber_bot=GreedyPlayer(name='Greedy2'), server_address=server_address,
                         chosen_team_index=1, session_name=session_name)
    ]

    for opponent in opponents:
        process = Process(target=opponent.start)
        process.start()

    rl_player = RLPlayer(name='rl_player', model_path=model_path)
    jass_challenge_bot = JassChallengeBot(pyschieber_bot=rl_player, server_address=server_address,
                                          session_name=session_name)
    jass_challenge_bot.start()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='SchieberJassBot', )
    parser.add_argument('-a', '--server_address', dest='server_address', help='Default: ws://127.0.0.1:3000')
    parser.add_argument('-s', '--session_name', dest='session_name', help='Default: test')
    parser.set_defaults(server_address="ws://127.0.0.1:3000", session_name="test")
    args = parser.parse_args()
    launch(args.server_address, args.session_name)
