from pyschieber.card import Card as PyschieberCard

from schieberjassbot.jass_challenge_wrapper.card import Card as ChallengeCard
from schieberjassbot.jass_challenge_wrapper.parser.color_parser import suit_to_color, color_to_suit


def pyscheiber_card_to_challenge_card(pyschieber_card):
    color = suit_to_color(pyschieber_card.suit)
    return ChallengeCard(color=color.name, number=pyschieber_card.value)


def challenge_card_to_pyschieber_card(challenge_card):
    suit = color_to_suit(challenge_card.color)
    return PyschieberCard(suit=suit, value=challenge_card.number)
