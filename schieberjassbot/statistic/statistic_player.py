import random
from statistics import mean

from pyschieber.card import from_string_to_card
from pyschieber.deck import Deck
from pyschieber.player.base_player import BasePlayer
from pyschieber.rules.stich_rules import allowed_cards
from pyschieber.trumpf import Trumpf


class StatisticPlayer(BasePlayer):
    def __init__(self, name='StatisticPlayer'):
        BasePlayer.__init__(self, name)
        self.unknown_cards = Deck().cards
        self.stiche = {1: [], 2: [], 3: [], 4: [], 5: [], 6: [], 7: [], 8: [], 9: []}
        self.stich_counter = 1

    def set_card(self, card):
        if len(self.cards) == 0:
            self.unknown_cards = Deck().cards
        self.cards.append(card)
        try:
            self.unknown_cards.remove(card)
        except:
            pass
        if len(self.cards) == 9:
            self.stich_counter = 1
            assert len(self.unknown_cards) == 27

    def move_made(self, player_id, card, _):
        try:
            self.unknown_cards.remove(card)
        except:
            pass

    def choose_trumpf(self, geschoben):
        return move(choices=list(Trumpf))

    def choose_card(self, state=None):
        self.stiche[self.stich_counter].append(self.allowed_unknown_cards(state=state))
        cards = self.allowed_cards(state=state)
        return move(choices=cards)

    def stich_over(self, state=None):
        self.stich_counter += 1

    def allowed_unknown_cards(self, state):
        table_cards = [from_string_to_card(entry['card']) for entry in state['table']]
        trumpf = Trumpf[state['trumpf']]
        return len(allowed_cards(hand_cards=self.unknown_cards, table_cards=table_cards, trumpf=trumpf))

    def print_statistic(self):
        print('\n')
        print('-' * 120)
        print('%-12s%-12s%-12s%-12s' % ('Stich', 'max', 'min', 'mean'))
        print('-' * 120)
        for stich_nr, values in self.stiche.items():
            print('%-12i%-12i%-12i%-12f' % (stich_nr, max(values), min(values), mean(values)))
        print('-' * 120)


def move(choices):
    allowed = False
    while not allowed:
        choice = random.choice(choices)
        allowed = yield choice
        if allowed:
            yield None
