import json
import random
from statistics import mean

from pyschieber.card import from_string_to_card
from pyschieber.deck import Deck
from pyschieber.player.base_player import BasePlayer
from pyschieber.rules.stich_rules import allowed_cards
from pyschieber.trumpf import Trumpf


class PermutationPlayer(BasePlayer):
    def __init__(self, name='StatisticPlayer'):
        BasePlayer.__init__(self, name)
        self.not_played_cards = Deck().cards
        self.stiche = {1: [], 2: [], 3: [], 4: [], 5: [], 6: [], 7: [], 8: [], 9: []}
        self.stich_counter = 1

    def set_card(self, card):
        if len(self.cards) == 0:
            self.not_played_cards = Deck().cards
        self.cards.append(card)
        if len(self.cards) == 9:
            self.stich_counter = 1

    def move_made(self, player_id, card, _):
         self.not_played_cards.remove(card)

    def choose_trumpf(self, geschoben):
        return move(choices=list(Trumpf))

    def choose_card(self, state=None):
        self.stiche[self.stich_counter].append(self.all_allowed_cards(state=state))
        cards = self.allowed_cards(state=state)
        return move(choices=cards)

    def stich_over(self, state=None):
        self.stich_counter += 1
        if self.stich_counter == 9:
            print()
            print(json.dumps(state))

    def all_allowed_cards(self, state):
        table_cards = [from_string_to_card(entry['card']) for entry in state['table']]
        trumpf = Trumpf[state['trumpf']]
        return len(allowed_cards(hand_cards=self.not_played_cards, table_cards=table_cards, trumpf=trumpf))

    def print_statistic(self):
        print('\n')
        print('-' * 120)
        print('%-12s%-12s%-12s%-12s' % ('Stich', 'max', 'min', 'mean'))
        print('-' * 120)
        for stich_nr, values in self.stiche.items():
            print('%-12i%-12i%-12i%-12f' % (stich_nr, max(values), min(values), mean(values)))
        print('-' * 120)


def move(choices):
    allowed = False
    while not allowed:
        choice = random.choice(choices)
        allowed = yield choice
        if allowed:
            yield None
